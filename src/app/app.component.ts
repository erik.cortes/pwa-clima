import { Component, OnInit } from '@angular/core';
import { WeatherService } from './service/weather.service';
import { ControlContainer, FormBuilder, FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import {AbstractControl} from '@angular/forms';
import { isEmptyExpression } from '@angular/compiler';
import { JsonpClientBackend } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[WeatherService]
})
export class AppComponent implements OnInit {
  
  weather;
  public clima:any;
  public unit:any;
  validacion: any = /[0-9-.]/;


  constructor(private weatherservice:WeatherService){
    this.unit =  {
      metric:'', 
      lat:'',
      lon:''
          }
          this.clima = {
            metric:'',
            lat: '',
            lon: ''
          }
           
          this.localstorage()

    
    

  }
  ngOnInit(){
   
  }
 
  getWeather(lat,lon,units){
     //getCoords void from weather.service.ts
   this.weatherservice.getCoords(lat, lon,units)

   
   //while we return getCoords data with this, we can use subscribe
    .subscribe(
      res => {
        console.log(res);
        this.weather = res
      },
      err => console.log(err)


      /*Returning data by console */
     /*  res => console.log(res),
      err => console.log(err) */
    )
    //localStorage.getItem(this.weather.name);
    
  }
  submitLocation(lat,lon,units){
    if(lat.value === ' '){
      alert('error')
    }
    else{

   
  
    if(lat.value && lon.value){
     
      
    this.getWeather(lat.value,lon.value,units.value);
   /*  console.log(lat.value,lon.value);//print by console */
   
     localStorage.setItem("localizacion",JSON.stringify(this.unit));
     this.localstorage();
     
  }else {

    alert('error en los campos, inserte valores');

  }
    return false;

  
  }
}
     localstorage(){
       
      this.clima = localStorage.getItem("localizacion");
      this.clima = JSON.parse(localStorage.getItem("localizacion"));
     }
    
    
  //   if()
  //   localStorage.setItem("localización",JSON.stringify(this.unit));

  // }
 
}


