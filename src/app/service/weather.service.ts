import {HttpClient,HttpParams} from '@angular/common/http'
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  url = 'https://api.openweathermap.org/data/2.5/weather';
  apikey = "685fb44315c4a831e484efbb03ce5195";
  lang = 'es';
  units ='';
  constructor(private http:HttpClient) {

   }
   getCoords(lat,lon,units){
    //set variables by httpparams
    let params = new HttpParams()
    .set('lat',lat)
    .set('lon',lon)
    .set('lang',this.lang)
    .set('units',units)
    .set('appid',this.apikey)
    
    return this.http.get(this.url, {params}); 
  }

}
