import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataweatherComponent } from './dataweather.component';

describe('DataweatherComponent', () => {
  let component: DataweatherComponent;
  let fixture: ComponentFixture<DataweatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataweatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataweatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
